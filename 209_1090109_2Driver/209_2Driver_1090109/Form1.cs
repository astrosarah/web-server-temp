﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


/// <summary>
/// Inclusion of PEAK PCAN-Basic namespace
/// </summary>
using Peak.Can.Basic;
using TPCANHandle = System.UInt16;
using TPCANBitrateFD = System.String;
using TPCANTimestampFD = System.UInt64;
using System.IO;
using System.Reflection;
using ChartDirectorSampleCode;
using System.Windows.Forms.DataVisualization.Charting;

namespace ICDIBasic
{
    public partial class Form1 : Form
    {
        private string ID_ARMY = "204h";
        private string ID_TCU = "214h";
    

        BindingSource BindingPara = new BindingSource();
        List<ColumnParams> itemsParam = new List<ColumnParams>();

        int readidx;

        int newX;

        public const int sampleSize = 10000;
        private double[] timeStamps = new double[sampleSize];
        private double[] dataSeriesA = new double[sampleSize];
        private double[] dataSeriesB = new double[sampleSize];
        private double[] dataSeriesC = new double[sampleSize];

        // Define some variables
        int numberOfPointsInChart = 1000;
        int numberOfPointsAfterRemoval = 1000;
        double msfactor = 0.01;

        Queue<DataPacket> dataQueue = new Queue<DataPacket>();
        DataPacket dataBuff = new DataPacket();
        private int currentIndex = 0;
        private DoubleBufferedQueue<DataPacket> buffer = new DoubleBufferedQueue<DataPacket>();
        private RandomWalk dataSource;
        ushort delay=10;
        private class DataPacket
        {
            public double elapsedTime;
            public double series0;
            public double series1;
            public double series2;
        };

        class ColumnParams
        {
            public int No { get; set; }
            public string ID { get; set; }
            public byte Byte0 { get; set; }
            public byte Byte1 { get; set; }
            public byte Byte2 { get; set; }
            public byte Byte3 { get; set; }
            public byte Byte4 { get; set; }
            public byte Byte5 { get; set; }
            public byte Byte6 { get; set; }
            public byte Byte7 { get; set; }

            public ColumnParams(
                int No,
                string ID,
                byte Byte0,
                byte Byte1,
                byte Byte2,
                byte Byte3,
                byte Byte4,
                byte Byte5,
                byte Byte6,
                byte Byte7
                )
            {
                this.No = No;
                this.ID = ID;
                this.Byte0 = Byte0;
                this.Byte1 = Byte1;
                this.Byte2 = Byte2;
                this.Byte3 = Byte3;
                this.Byte4 = Byte4;
                this.Byte5 = Byte5;
                this.Byte6 = Byte6;
                this.Byte7 = Byte7;
            }
        }


        #region Structures
        /// <summary>
        /// Message Status structure used to show CAN Messages
        /// in a ListView
        /// </summary>
        private class MessageStatus
        {
            private TPCANMsgFD m_Msg;
            private TPCANTimestampFD m_TimeStamp;
            private TPCANTimestampFD m_oldTimeStamp;
            private int m_iIndex;
            private int m_Count;
            private bool m_bShowPeriod;
            private bool m_bWasChanged;

            public MessageStatus(TPCANMsgFD canMsg, TPCANTimestampFD canTimestamp, int listIndex)
            {
                m_Msg = canMsg;
                m_TimeStamp = canTimestamp;
                m_oldTimeStamp = canTimestamp;
                m_iIndex = listIndex;
                m_Count = 1;
                m_bShowPeriod = true;
                m_bWasChanged = false;
            }

            public void Update(TPCANMsgFD canMsg, TPCANTimestampFD canTimestamp)
            {
                m_Msg = canMsg;
                m_oldTimeStamp = m_TimeStamp;
                m_TimeStamp = canTimestamp;
                m_bWasChanged = true;
                m_Count += 1;
            }

            public TPCANMsgFD CANMsg
            {
                get { return m_Msg; }
            }

            public TPCANTimestampFD Timestamp
            {
                get { return m_TimeStamp; }
            }

            public int Position
            {
                get { return m_iIndex; }
            }

            public string TypeString
            {
                get { return GetMsgTypeString(); }
            }

            public string IdString
            {
                get { return GetIdString(); }
            }

            public string DataString
            {
                get { return GetDataString(); }
            }

            public int Count
            {
                get { return m_Count; }
            }

            public bool ShowingPeriod
            {
                get { return m_bShowPeriod; }
                set
                {
                    if (m_bShowPeriod ^ value)
                    {
                        m_bShowPeriod = value;
                        m_bWasChanged = true;
                    }
                }
            }

            public bool MarkedAsUpdated
            {
                get { return m_bWasChanged; }
                set { m_bWasChanged = value; }
            }

            public string TimeString
            {
                get { return GetTimeString(); }
            }

            private string GetTimeString()
            {
                double fTime;

                fTime = (m_TimeStamp / 1000.0);
                if (m_bShowPeriod)
                    fTime -= (m_oldTimeStamp / 1000.0);
                return fTime.ToString("F1");
            }

            private string GetDataString()
            {
                string strTemp;

                strTemp = "";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_RTR) == TPCANMessageType.PCAN_MESSAGE_RTR)
                    return "Remote Request";
                else
                    for (int i = 0; i < Form1.GetLengthFromDLC(m_Msg.DLC, (m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == 0); i++)
                        strTemp += string.Format("{0:X2} ", m_Msg.DATA[i]);

                return strTemp;
            }

            private string GetIdString()
            {
                // We format the ID of the message and show it
                //
                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_EXTENDED) == TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                    return string.Format("{0:X8}h", m_Msg.ID);
                else
                    return string.Format("{0:X3}h", m_Msg.ID);
            }

            private string GetMsgTypeString()
            {
                string strTemp;

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_STATUS) == TPCANMessageType.PCAN_MESSAGE_STATUS)
                    return "STATUS";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_ERRFRAME) == TPCANMessageType.PCAN_MESSAGE_ERRFRAME)
                    return "ERROR";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_EXTENDED) == TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                    strTemp = "EXT";
                else
                    strTemp = "STD";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_RTR) == TPCANMessageType.PCAN_MESSAGE_RTR)
                    strTemp += "/RTR";
                else
                    if ((int)m_Msg.MSGTYPE > (int)TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                {
                    strTemp += " [ ";
                    if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == TPCANMessageType.PCAN_MESSAGE_FD)
                        strTemp += " FD";
                    if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_BRS) == TPCANMessageType.PCAN_MESSAGE_BRS)
                        strTemp += " BRS";
                    if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_ESI) == TPCANMessageType.PCAN_MESSAGE_ESI)
                        strTemp += " ESI";
                    strTemp += " ]";
                }

                return strTemp;
            }

        }
        #endregion

        #region Delegates
        /// <summary>
        /// Read-Delegate Handler
        /// </summary>
        private delegate void ReadDelegateHandler();
        #endregion

        #region Members
        /// <summary>
        /// Saves the desired connection mode
        /// </summary>
        private bool m_IsFD;
        /// <summary>
        /// Saves the handle of a PCAN hardware
        /// </summary>
        private TPCANHandle m_PcanHandle;
        /// <summary>
        /// Saves the baudrate register for a conenction
        /// </summary>
        private TPCANBaudrate m_Baudrate;
        /// <summary>
        /// Saves the type of a non-plug-and-play hardware
        /// </summary>
        private TPCANType m_HwType;
        /// <summary>
        /// Stores the status of received messages for its display
        /// </summary>
        private System.Collections.ArrayList m_LastMsgsList;
        /// <summary>
        /// Read Delegate for calling the function "ReadMessages"
        /// </summary>
        private ReadDelegateHandler m_ReadDelegate;
        /// <summary>
        /// Receive-Event
        /// </summary>
        private System.Threading.AutoResetEvent m_ReceiveEvent;
        /// <summary>
        /// Thread for message reading (using events)
        /// </summary>
        private System.Threading.Thread m_ReadThread;
        /// <summary>
        /// Handles of the current available PCAN-Hardware
        /// </summary>
        private TPCANHandle[] m_HandlesArray;
        #endregion

        #region Methods
        #region Help functions
        /// <summary>
        /// Convert a CAN DLC value into the actual data length of the CAN/CAN-FD frame.
        /// </summary>
        /// <param name="dlc">A value between 0 and 15 (CAN and FD DLC range)</param>
        /// <param name="isSTD">A value indicating if the msg is a standard CAN (FD Flag not checked)</param>
        /// <returns>The length represented by the DLC</returns>
        public static int GetLengthFromDLC(int dlc, bool isSTD)
        {
            if (dlc <= 8)
                return dlc;

            if (isSTD)
                return 8;

            switch (dlc)
            {
                case 9: return 12;
                case 10: return 16;
                case 11: return 20;
                case 12: return 24;
                case 13: return 32;
                case 14: return 48;
                case 15: return 64;
                default: return dlc;
            }
        }

        /// <summary>
        /// Initialization of PCAN-Basic components
        /// </summary>
        private void InitializeBasicComponents()
        {
            // Creates the list for received messages
            //
            m_LastMsgsList = new System.Collections.ArrayList();
            // Creates the delegate used for message reading
            //
            m_ReadDelegate = new ReadDelegateHandler(ReadMessages);
            // Creates the event used for signalize incomming messages 
            //
            m_ReceiveEvent = new System.Threading.AutoResetEvent(false);
            // Creates an array with all possible PCAN-Channels
            //
            m_HandlesArray = new TPCANHandle[]
            {
                PCANBasic.PCAN_ISABUS1,
                PCANBasic.PCAN_ISABUS2,
                PCANBasic.PCAN_ISABUS3,
                PCANBasic.PCAN_ISABUS4,
                PCANBasic.PCAN_ISABUS5,
                PCANBasic.PCAN_ISABUS6,
                PCANBasic.PCAN_ISABUS7,
                PCANBasic.PCAN_ISABUS8,
                PCANBasic.PCAN_DNGBUS1,
                PCANBasic.PCAN_PCIBUS1,
                PCANBasic.PCAN_PCIBUS2,
                PCANBasic.PCAN_PCIBUS3,
                PCANBasic.PCAN_PCIBUS4,
                PCANBasic.PCAN_PCIBUS5,
                PCANBasic.PCAN_PCIBUS6,
                PCANBasic.PCAN_PCIBUS7,
                PCANBasic.PCAN_PCIBUS8,
                PCANBasic.PCAN_PCIBUS9,
                PCANBasic.PCAN_PCIBUS10,
                PCANBasic.PCAN_PCIBUS11,
                PCANBasic.PCAN_PCIBUS12,
                PCANBasic.PCAN_PCIBUS13,
                PCANBasic.PCAN_PCIBUS14,
                PCANBasic.PCAN_PCIBUS15,
                PCANBasic.PCAN_PCIBUS16,
                PCANBasic.PCAN_USBBUS1,
                PCANBasic.PCAN_USBBUS2,
                PCANBasic.PCAN_USBBUS3,
                PCANBasic.PCAN_USBBUS4,
                PCANBasic.PCAN_USBBUS5,
                PCANBasic.PCAN_USBBUS6,
                PCANBasic.PCAN_USBBUS7,
                PCANBasic.PCAN_USBBUS8,
                PCANBasic.PCAN_USBBUS9,
                PCANBasic.PCAN_USBBUS10,
                PCANBasic.PCAN_USBBUS11,
                PCANBasic.PCAN_USBBUS12,
                PCANBasic.PCAN_USBBUS13,
                PCANBasic.PCAN_USBBUS14,
                PCANBasic.PCAN_USBBUS15,
                PCANBasic.PCAN_USBBUS16,
                PCANBasic.PCAN_PCCBUS1,
                PCANBasic.PCAN_PCCBUS2,
                PCANBasic.PCAN_LANBUS1,
                PCANBasic.PCAN_LANBUS2,
                PCANBasic.PCAN_LANBUS3,
                PCANBasic.PCAN_LANBUS4,
                PCANBasic.PCAN_LANBUS5,
                PCANBasic.PCAN_LANBUS6,
                PCANBasic.PCAN_LANBUS7,
                PCANBasic.PCAN_LANBUS8,
                PCANBasic.PCAN_LANBUS9,
                PCANBasic.PCAN_LANBUS10,
                PCANBasic.PCAN_LANBUS11,
                PCANBasic.PCAN_LANBUS12,
                PCANBasic.PCAN_LANBUS13,
                PCANBasic.PCAN_LANBUS14,
                PCANBasic.PCAN_LANBUS15,
                PCANBasic.PCAN_LANBUS16,
            };

            // Fills and configures the Data of several comboBox components
            //
            //   FillComboBoxData();

            // Prepares the PCAN-Basic's debug-Log file
            //
            //   ConfigureLogFile();
        }

        /// <summary>
        /// Help Function used to get an error as text
        /// </summary>
        /// <param name="error">Error code to be translated</param>
        /// <returns>A text with the translated error</returns>
        private string GetFormatedError(TPCANStatus error)
        {
            StringBuilder strTemp;

            // Creates a buffer big enough for a error-text
            //
            strTemp = new StringBuilder(256);
            // Gets the text using the GetErrorText API function
            // If the function success, the translated error is returned. If it fails,
            // a text describing the current error is returned.
            //
            if (PCANBasic.GetErrorText(error, 0, strTemp) != TPCANStatus.PCAN_ERROR_OK)
                return string.Format("An error occurred. Error-code's text ({0:X}) couldn't be retrieved", error);
            else
                return strTemp.ToString();
        }

        /// <summary>
        /// Includes a new line of text into the information Listview
        /// </summary>
        /// <param name="strMsg">Text to be included</param>
        private void IncludeTextMessage(string strMsg)
        {
        }

        /// <summary>
        /// Gets the current status of the PCAN-Basic message filter
        /// </summary>
        /// <param name="status">Buffer to retrieve the filter status</param>
        /// <returns>If calling the function was successfull or not</returns>
        private bool GetFilterStatus(out uint status)
        {
            TPCANStatus stsResult;

            // Tries to get the sttaus of the filter for the current connected hardware
            //
            stsResult = PCANBasic.GetValue(m_PcanHandle, TPCANParameter.PCAN_MESSAGE_FILTER, out status, sizeof(UInt32));

            // If it fails, a error message is shown
            //
            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
            {
                MessageBox.Show(GetFormatedError(stsResult));
                return false;
            }
            return true;
        }



        /// <summary>
        /// Activates/deaactivates the different controls of the main-form according
        /// with the current connection status
        /// </summary>
        /// <param name="bConnected">Current status. True if connected, false otherwise</param>
        private void SetConnectionStatus(bool bConnected)
        {
            // Buttons
            //
            btnInit.Enabled = !bConnected;
            btnRead.Enabled = bConnected && rdbManual.Checked;
            btnRelease.Enabled = bConnected;
            btnFilterApply.Enabled = bConnected;
            btnFilterQuery.Enabled = bConnected;
            // ComboBoxs
            //
            cbbBaudrates.Enabled = !bConnected;

            // Check-Buttons
            //
            //   chbCanFD.Enabled = !bConnected;

            // Hardware configuration and read mode
            //
            if (!bConnected)
            {
                //    cbbChannel_SelectedIndexChanged(this, new EventArgs());
            }

            else
                rdbTimer_CheckedChanged(this, new EventArgs());

            // Display messages in grid
            //
            tmrDisplay.Enabled = bConnected;
        }

        #endregion

        #region Message-proccessing functions
        /// <summary>
        /// Display CAN messages in the Message-ListView
        /// </summary>
        private void DisplayMessages()
        {
            byte[] arrDatabyte = new byte[9];


            ListViewItem lviCurrentItem;
            readidx++;

            lock (m_LastMsgsList.SyncRoot)
            {
                foreach (MessageStatus msgStatus in m_LastMsgsList)
                {
                    // Get the data to actualize
                    //
                    if (msgStatus.MarkedAsUpdated)
                    {
                        msgStatus.MarkedAsUpdated = false;
                        lviCurrentItem = lstMessages.Items[msgStatus.Position];

                        lviCurrentItem.SubItems[2].Text = GetLengthFromDLC(msgStatus.CANMsg.DLC, (msgStatus.CANMsg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == 0).ToString();
                        lviCurrentItem.SubItems[3].Text = msgStatus.Count.ToString();
                        lviCurrentItem.SubItems[4].Text = msgStatus.TimeString;
                        lviCurrentItem.SubItems[5].Text = msgStatus.DataString;

                        for (int i = 0; i < 8; i++)
                        {

                            arrDatabyte[i] = msgStatus.CANMsg.DATA[i];
                        }

                       itemsParam.Insert(0, new ColumnParams(readidx, msgStatus.IdString, arrDatabyte[0], arrDatabyte[1], arrDatabyte[2], arrDatabyte[3], arrDatabyte[4], arrDatabyte[5], arrDatabyte[6], arrDatabyte[7]));

                        CAN_RXAnalysisMessage(arrDatabyte, msgStatus.IdString);
                        
                    }
                }
            }

           // var bindingList = new BindingList<ColumnParams>(itemsParam);
         //   var source = new BindingSource(bindingList, null);

          //  dgvParam.DataSource = source;
        }

        double[] arrCurrents = new double[50];
        int RMSIdx = 0;

        double[] arrVolts = new double[50];
        int RMSVIdx = 0;

        void CAN_RXAnalysisMessage(byte[] sRXMsgData, string ui32ObjID)
        {
            switch (ui32ObjID)
            {
                case "ID_ARMY":  // Read MCU Message then to Split depend bits

                    GlobalVarParameter.TCU_ChkSum = Convert.ToInt16((sRXMsgData[0] & 0xF0) >> 4); // 4 bits (63~60)
                                                                                                  // GlobalVarParameter.TCU_ChkSum = Convert.ToInt16(sRXMsgData[0]); // 4 bits (63~60)     for test
                    GlobalVarParameter.TCU_RolCnt = Convert.ToInt16(sRXMsgData[0] & 0x0F);      // 4 bits (59~56)
                    GlobalVarParameter.TCU_Tor_slew_rate = Convert.ToInt16(sRXMsgData[1] << 2 | (sRXMsgData[2] & 0xC0) >> 6);     // 10 bits (55~46)
                    GlobalVarParameter.TCU_shift_status = Convert.ToInt16((sRXMsgData[2] & 0x38) >> 3); // 3 bits (45~43)
                    GlobalVarParameter.TCU_Gear_status = Convert.ToInt16((sRXMsgData[2] & 0x6) >> 1);  // 2 bits (42~41)
                    GlobalVarParameter.TCU_to_MCU_control_mode = Convert.ToInt16((sRXMsgData[2] & 0x1)); // 1 bits (40~40)

                    break;

                case "ID_TCU":
                    GlobalVarParameter.AIRead_1 = Convert.ToInt16((sRXMsgData[0] & 0xFF) << 8 | sRXMsgData[1]); // 4 bits (63~60)
                    GlobalVarParameter.AIRead_2 = Convert.ToInt16((sRXMsgData[2] & 0xFF) << 8 | sRXMsgData[3]);      // 4 bits (59~56)
                    GlobalVarParameter.DIRead = Convert.ToInt16((sRXMsgData[4] & 0xFF));
                    break;

                case "224h":

                    break;
            }

            foreach (DataGridViewRow row in dgvParam.Rows)
            {
                if (row.Cells[1].Value != null)
                {
                    if (row.Cells[1].Value.ToString().Equals("AIRead_1"))  //current
                    {
                        //1081226 將數據RMS並一秒更新一次
                        if (RMSIdx < 50)
                        {
                            row.Cells[2].Value = GlobalVarParameter.AIRead_1;
                            GlobalVarParameter.Current = (Convert.ToDouble(GlobalVarParameter.AIRead_1) - 2117) * 0.111;
                           // txtCurrent.Text = GlobalVarParameter.Current.ToString("f1");
                            arrCurrents[RMSIdx] = GlobalVarParameter.Current;
                            RMSIdx++;
                        }
                        if(RMSIdx==50)
                        {
                            float RMSCurrent;
                            RMSCurrent = RMS_Value(arrCurrents, 50);
                            txtCurrent.Text = RMSCurrent.ToString("f1");
                            RMSIdx = 0;
                        }

                    }
                    if (row.Cells[1].Value.ToString().Equals("AIRead_2"))
                    {
                        //1081226 將數據RMS並一秒更新一次
                        if (RMSVIdx < 50)
                        {
                            row.Cells[2].Value = GlobalVarParameter.AIRead_2;
                            GlobalVarParameter.Volt = (Convert.ToDouble(GlobalVarParameter.AIRead_2) - GlobalVarParameter.Offset) * GlobalVarParameter.Gain;
                            //txtVolt.Text = GlobalVarParameter.Volt.ToString("f1");
                            arrVolts[RMSVIdx] = GlobalVarParameter.Volt;
                            RMSVIdx++;
                        }
                        if (RMSVIdx == 50)
                        {
                            float RMSVolt;
                            RMSVolt = RMS_Value(arrVolts, 50);
                            txtVolt.Text = RMSVolt.ToString("f1");
                            RMSVIdx = 0;
                        }
                    }
                        
                    if (row.Cells[1].Value.ToString().Equals("DIRead"))   //1080313
                    {
                    }
                }
            }

        }


        // Root Mean Square  
        static float RMS_Value(double[] arr, int n)
        {
            int square = 0;
            float mean, root = 0;

            // Calculate square
            for (int i = 0; i < n; i++)
            {
                square += (int)Math.Pow(arr[i], 2);
            }

            // Calculate Mean
            mean = (square / (float)(n));

            // Calculate Root
            root = (float)Math.Sqrt(mean);

            return root;
        }


        /// <summary>
        /// Inserts a new entry for a new message in the Message-ListView
        /// </summary>
        /// <param name="newMsg">The messasge to be inserted</param>
        /// <param name="timeStamp">The Timesamp of the new message</param>
        private void InsertMsgEntry(TPCANMsgFD newMsg, TPCANTimestampFD timeStamp)
        {
            MessageStatus msgStsCurrentMsg;
            ListViewItem lviCurrentItem;

            lock (m_LastMsgsList.SyncRoot)
            {
                // We add this status in the last message list
                //
                msgStsCurrentMsg = new MessageStatus(newMsg, timeStamp, lstMessages.Items.Count);
                msgStsCurrentMsg.ShowingPeriod = chbShowPeriod.Checked;
                m_LastMsgsList.Add(msgStsCurrentMsg);

                // Add the new ListView Item with the Type of the message
                //	
                lviCurrentItem = lstMessages.Items.Add(msgStsCurrentMsg.TypeString);
                // We set the ID of the message
                //
                lviCurrentItem.SubItems.Add(msgStsCurrentMsg.IdString);
                // We set the length of the Message
                //
                lviCurrentItem.SubItems.Add(GetLengthFromDLC(newMsg.DLC, (newMsg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == 0).ToString());
                // we set the message count message (this is the First, so count is 1)            
                //
                lviCurrentItem.SubItems.Add(msgStsCurrentMsg.Count.ToString());
                // Add time stamp information if needed
                //
                lviCurrentItem.SubItems.Add(msgStsCurrentMsg.TimeString);
                // We set the data of the message. 	
                //
                lviCurrentItem.SubItems.Add(msgStsCurrentMsg.DataString);
            }
        }

        /// <summary>
        /// Processes a received message, in order to show it in the Message-ListView
        /// </summary>
        /// <param name="theMsg">The received PCAN-Basic message</param>
        /// <returns>True if the message must be created, false if it must be modified</returns>
        private void ProcessMessage(TPCANMsgFD theMsg, TPCANTimestampFD itsTimeStamp)
        {
            // We search if a message (Same ID and Type) is 
            // already received or if this is a new message
            //
            lock (m_LastMsgsList.SyncRoot)
            {
                foreach (MessageStatus msg in m_LastMsgsList)
                {
                    if ((msg.CANMsg.ID == theMsg.ID) && (msg.CANMsg.MSGTYPE == theMsg.MSGTYPE))
                    {
                        // Modify the message and exit
                        //
                        msg.Update(theMsg, itsTimeStamp);
                        return;
                    }
                }
                // Message not found. It will created
                //
                InsertMsgEntry(theMsg, itsTimeStamp);
            }
        }

        /// <summary>
        /// Processes a received message, in order to show it in the Message-ListView
        /// </summary>
        /// <param name="theMsg">The received PCAN-Basic message</param>
        /// <returns>True if the message must be created, false if it must be modified</returns>
        private void ProcessMessage(TPCANMsg theMsg, TPCANTimestamp itsTimeStamp)
        {
            TPCANMsgFD newMsg;
            TPCANTimestampFD newTimestamp;

            newMsg = new TPCANMsgFD();
            newMsg.DATA = new byte[64];
            newMsg.ID = theMsg.ID;
            newMsg.DLC = theMsg.LEN;
            for (int i = 0; i < ((theMsg.LEN > 8) ? 8 : theMsg.LEN); i++)
                newMsg.DATA[i] = theMsg.DATA[i];
            newMsg.MSGTYPE = theMsg.MSGTYPE;

            newTimestamp = Convert.ToUInt64(itsTimeStamp.micros + 1000 * itsTimeStamp.millis + 0x100000000 * 1000 * itsTimeStamp.millis_overflow);
            ProcessMessage(newMsg, newTimestamp);
        }

        /// <summary>
        /// Thread-Function used for reading PCAN-Basic messages
        /// </summary>
        private void CANReadThreadFunc()
        {
            UInt32 iBuffer;
            TPCANStatus stsResult;

            iBuffer = Convert.ToUInt32(m_ReceiveEvent.SafeWaitHandle.DangerousGetHandle().ToInt32());
            // Sets the handle of the Receive-Event.
            //
            stsResult = PCANBasic.SetValue(m_PcanHandle, TPCANParameter.PCAN_RECEIVE_EVENT, ref iBuffer, sizeof(UInt32));

            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
            {
                MessageBox.Show(GetFormatedError(stsResult), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// Function for reading messages on FD devices
        /// </summary>
        /// <returns>A TPCANStatus error code</returns>
        private TPCANStatus ReadMessageFD()
        {
            TPCANMsgFD CANMsg;
            TPCANTimestampFD CANTimeStamp;
            TPCANStatus stsResult;

            // We execute the "Read" function of the PCANBasic                
            //
            stsResult = PCANBasic.ReadFD(m_PcanHandle, out CANMsg, out CANTimeStamp);
            if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                // We process the received message
                //
                ProcessMessage(CANMsg, CANTimeStamp);

            return stsResult;
        }

        /// <summary>
        /// Function for reading CAN messages on normal CAN devices
        /// </summary>
        /// <returns>A TPCANStatus error code</returns>
        private TPCANStatus ReadMessage()
        {
            TPCANMsg CANMsg;
            TPCANTimestamp CANTimeStamp;
            TPCANStatus stsResult;

            // We execute the "Read" function of the PCANBasic                
            //
            stsResult = PCANBasic.Read(m_PcanHandle, out CANMsg, out CANTimeStamp);
            if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                // We process the received message
                //
                ProcessMessage(CANMsg, CANTimeStamp);

            return stsResult;
        }

        /// <summary>
        /// Function for reading PCAN-Basic messages
        /// </summary>
        private void ReadMessages()
        {
            TPCANStatus stsResult;

            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //			
            do
            {
                stsResult = m_IsFD ? ReadMessageFD() : ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;
            } while (btnRelease.Enabled && (!Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));
        }
        #endregion

        #region Event Handlers
        #region Form event-handlers
        /// <summary>
        /// Consturctor
        /// </summary>
        public Form1()
        {
            // Initializes Form's component
            //
            InitializeComponent();
            // Initializes specific components
            //
            InitializeBasicComponents();
        }

        /// <summary>
        /// Form-Closing Function / Finish function
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Releases the used PCAN-Basic channel
            //
            if (btnRelease.Enabled)
                btnRelease_Click(this, new EventArgs());

            PCANBasic.Uninitialize(m_PcanHandle);
        }


        #endregion

        #region ComboBox event-handlers

        private void cbbBaudrates_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Saves the current selected baudrate register code
            //
            switch (cbbBaudrates.SelectedIndex)
            {
                case 0:
                    m_Baudrate = TPCANBaudrate.PCAN_BAUD_1M;
                    break;
                case 1:
                    m_Baudrate = TPCANBaudrate.PCAN_BAUD_800K;
                    break;
                case 2:
                    m_Baudrate = TPCANBaudrate.PCAN_BAUD_500K;
                    break;
                case 3:
                    m_Baudrate = TPCANBaudrate.PCAN_BAUD_250K;
                    break;
                case 4:
                    m_Baudrate = TPCANBaudrate.PCAN_BAUD_125K;
                    break;
                case 5:
                    m_Baudrate = TPCANBaudrate.PCAN_BAUD_100K;
                    break;
            }
        }

        #endregion

        #region Button event-handlers


        private void btnInit_Click(object sender, EventArgs e)
        {
            TPCANStatus stsResult;

            // Connects a selected PCAN-Basic channel
            //
            stsResult = PCANBasic.Initialize(m_PcanHandle, m_Baudrate, 0, 0, 0);

            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
                if (stsResult != TPCANStatus.PCAN_ERROR_CAUTION)
                    MessageBox.Show(GetFormatedError(stsResult));
                else
                {
                    IncludeTextMessage("******************************************************");
                    IncludeTextMessage("The bitrate being used is different than the given one");
                    IncludeTextMessage("******************************************************");
                    stsResult = TPCANStatus.PCAN_ERROR_OK;
                }
            else
            {
                // Prepares the PCAN-Basic's PCAN-Trace file
                //
                //  ConfigureTraceFile();

                // Sets the connection status of the main-form
                //
                SetConnectionStatus(stsResult == TPCANStatus.PCAN_ERROR_OK);

                //clear chart1 data & Axis
                foreach (Series sr in chart1.Series)
                {
                    sr.ResetIsValueShownAsLabel();
                    sr.Points.Clear();
                }

                // 初始化Chart AxisX Min=0 Max=5D    & Buffer data            
                chart1.ChartAreas[0].AxisX.Maximum = 10D;  //一開始直到跑到100才開始移動
                chart1.ChartAreas[1].AxisX.Maximum = 10D;  //一開始直到跑到100才開始移動
                //chart1.ChartAreas[2].AxisX.Maximum = 10D;  //一開始直到跑到100才開始移動

                chart1.ChartAreas[0].AxisX.Minimum = 0D;  //一開始直到跑到100才開始移動
                chart1.ChartAreas[1].AxisX.Minimum = 0D;  //一開始直到跑到100才開始移動
               // chart1.ChartAreas[2].AxisX.Minimum = 0D;  //一開始直到跑到100才開始移動

                Array.Clear(timeStamps, 0, timeStamps.Length);
                Array.Clear(dataSeriesA, 0, timeStamps.Length);
                Array.Clear(dataSeriesB, 0, timeStamps.Length);
                Array.Clear(dataSeriesC, 0, timeStamps.Length);
                buffer = new DoubleBufferedQueue<DataPacket>();
                newX = 0;
                currentIndex = 0;
                // 初始化Chart Axis Min=0 Max=100    & Buffer data            

                string path = Directory.GetCurrentDirectory();
                string sfilename = DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt";
                if (sfilename != "")
                {
                    string date = DateTime.Now.ToString("yy-MM-dd-HH:mm");
                    String s = String.Format("{0,-15}  {1,-15}  {2,-15}  ", "Time", "Series0", "Series1", "Series2");

                    sw = new StreamWriter(sfilename);
                    sw.WriteLine(s);
                    sw.WriteLine(date);
                    sw.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                }

                // Start to get CANBUS data Receiver
                dataSource = new RandomWalk(onData);
                dataSource.start();

                // Now can start the timers for data collection and chart update
                chartUpdateTimer.Interval = delay;
                chartUpdateTimer.Start();

            }
        }
        // The index of the array position to which new data values are added.
        //private int currentIndex = 0;
        //private DoubleBufferedQueue<DataPacket> buffer = new DoubleBufferedQueue<DataPacket>();
        private void onData(double elapsedTime, double series0, double series1, double series2)
        {
            DataPacket p = new DataPacket();
            p.elapsedTime = elapsedTime;
            p.series0 = series0;
            p.series1 = series1;
            p.series2 = series2;
            buffer.put(p);
        }
        private void btnRelease_Click(object sender, EventArgs e)
        {
            // Releases a current connected PCAN-Basic channel
            //
            PCANBasic.Uninitialize(m_PcanHandle);
            tmrRead.Enabled = false;
            if (m_ReadThread != null)
            {
                m_ReadThread.Abort();
                m_ReadThread.Join();
                m_ReadThread = null;
            }

            // Sets the connection status of the main-form
            //
            SetConnectionStatus(false);

            try
            {
                if (null != dataSource)
                    dataSource.stop();

                if (sw != null)
                    sw.Close();

            }
            catch
            {
                MessageBox.Show("Release Error");
            }
        }

        private void btnFilterApply_Click(object sender, EventArgs e)
        {
            UInt32 iBuffer;
            TPCANStatus stsResult;

            // Gets the current status of the message filter
            //
            if (!GetFilterStatus(out iBuffer))
                return;

            // Configures the message filter for a custom range of messages
            //
            if (rdbFilterCustom.Checked)
            {
                // Sets the custom filter
                //
                stsResult = PCANBasic.FilterMessages(
                m_PcanHandle,
                Convert.ToUInt32(nudIdFrom.Value),
                Convert.ToUInt32(nudIdTo.Value),
                chbFilterExt.Checked ? TPCANMode.PCAN_MODE_EXTENDED : TPCANMode.PCAN_MODE_STANDARD);
                // If success, an information message is written, if it is not, an error message is shown
                //
                if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                    IncludeTextMessage(string.Format("The filter was customized. IDs from {0:X} to {1:X} will be received", nudIdFrom.Text, nudIdTo.Text));
                else
                    MessageBox.Show(GetFormatedError(stsResult));

                return;
            }

            // The filter will be full opened or complete closed
            //
            if (rdbFilterClose.Checked)
                iBuffer = PCANBasic.PCAN_FILTER_CLOSE;
            else
                iBuffer = PCANBasic.PCAN_FILTER_OPEN;

            // The filter is configured
            //
            stsResult = PCANBasic.SetValue(
                m_PcanHandle,
                TPCANParameter.PCAN_MESSAGE_FILTER,
                ref iBuffer,
                sizeof(UInt32));

            // If success, an information message is written, if it is not, an error message is shown
            //
            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                IncludeTextMessage(string.Format("The filter was successfully {0}", rdbFilterClose.Checked ? "closed." : "opened."));
            else
                MessageBox.Show(GetFormatedError(stsResult));
        }

        private void btnFilterQuery_Click(object sender, EventArgs e)
        {
            UInt32 iBuffer;

            // Queries the current status of the message filter
            //
            if (GetFilterStatus(out iBuffer))
            {
                switch (iBuffer)
                {
                    // The filter is closed
                    //
                    case PCANBasic.PCAN_FILTER_CLOSE:
                        IncludeTextMessage("The Status of the filter is: closed.");
                        break;
                    // The filter is fully opened
                    //
                    case PCANBasic.PCAN_FILTER_OPEN:
                        IncludeTextMessage("The Status of the filter is: full opened.");
                        break;
                    // The filter is customized
                    //
                    case PCANBasic.PCAN_FILTER_CUSTOM:
                        IncludeTextMessage("The Status of the filter is: customized.");
                        break;
                    // The status of the filter is undefined. (Should never happen)
                    //
                    default:
                        IncludeTextMessage("The Status of the filter is: Invalid.");
                        break;
                }
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            TPCANStatus stsResult;

            // We execute the "Read" function of the PCANBasic                
            //
            stsResult = m_IsFD ? ReadMessageFD() : ReadMessage();
            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
                // If an error occurred, an information message is included
                //
                IncludeTextMessage(GetFormatedError(stsResult));

            dgvParam.DataSource = itemsParam;
        }


        private void btnMsgClear_Click(object sender, EventArgs e)
        {
            // The information contained in the messages List-View
            // is cleared
            //
            lock (m_LastMsgsList.SyncRoot)
            {
                m_LastMsgsList.Clear();
                lstMessages.Items.Clear();
            }
        }

        private void btnInfoClear_Click(object sender, EventArgs e)
        {
        }

        private TPCANStatus WriteFrame()
        {
            TPCANMsg CANMsg;

            // We create a TPCANMsg message structure 
            //
            CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];

            // We configurate the Message.  The ID,
            // Length of the Data, Message Type
            // and the data
            //
            CANMsg.ID = Convert.ToUInt32("214", 16);
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE =  TPCANMessageType.PCAN_MESSAGE_STANDARD;
            // If a remote frame will be sent, the data bytes are not important.
            // We get so much data as the Len of the message
            //
            /*  for (int i = 0; i < GetLengthFromDLC(CANMsg.LEN, true); i++)
              {
                  txtbCurrentTextBox = (TextBox)this.Controls.Find("txtData" + i.ToString(), true)[0];
                  CANMsg.DATA[i] = Convert.ToByte(txtbCurrentTextBox.Text, 16);
              } */

            // The message is sent to the configured hardware
            CANMsg.DATA[0] = (byte)((GlobalVarParameter.AIRead_1 & 0x0F));
            CANMsg.DATA[1] = (byte)((GlobalVarParameter.AIRead_2 & 0x3FC) >> 2);
            CANMsg.DATA[2] = 0;
            CANMsg.DATA[4] = 0;
            CANMsg.DATA[5] = 0;
            CANMsg.DATA[6] = 0;
            CANMsg.DATA[7] = 0;

            return PCANBasic.Write(m_PcanHandle, ref CANMsg);
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            TPCANStatus stsResult;

            // Resets the receive and transmit queues of a PCAN Channel.
            //
            stsResult = PCANBasic.Reset(m_PcanHandle);

            // If it fails, a error message is shown
            //
            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
                MessageBox.Show(GetFormatedError(stsResult));
            else
                IncludeTextMessage("Receive and transmit queues successfully reset");
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            TPCANStatus stsResult;
            String errorName;

            // Gets the current BUS status of a PCAN Channel.
            //
            stsResult = PCANBasic.GetStatus(m_PcanHandle);

            // Switch On Error Name
            //
            switch (stsResult)
            {
                case TPCANStatus.PCAN_ERROR_INITIALIZE:
                    errorName = "PCAN_ERROR_INITIALIZE";
                    break;

                case TPCANStatus.PCAN_ERROR_BUSLIGHT:
                    errorName = "PCAN_ERROR_BUSLIGHT";
                    break;

                case TPCANStatus.PCAN_ERROR_BUSHEAVY: // TPCANStatus.PCAN_ERROR_BUSWARNING
                    errorName = m_IsFD ? "PCAN_ERROR_BUSWARNING" : "PCAN_ERROR_BUSHEAVY";
                    break;

                case TPCANStatus.PCAN_ERROR_BUSPASSIVE:
                    errorName = "PCAN_ERROR_BUSPASSIVE";
                    break;

                case TPCANStatus.PCAN_ERROR_BUSOFF:
                    errorName = "PCAN_ERROR_BUSOFF";
                    break;

                case TPCANStatus.PCAN_ERROR_OK:
                    errorName = "PCAN_ERROR_OK";
                    break;

                default:
                    errorName = "See Documentation";
                    break;
            }

            // Display Message
            //
            IncludeTextMessage(String.Format("Status: {0} ({1:X}h)", errorName, stsResult));
        }
        #endregion

        #region Timer event-handler
        private void tmrRead_Tick(object sender, EventArgs e)
        {
            // Checks if in the receive-queue are currently messages for read
            //             
            ReadMessages();
        }

        private void tmrDisplay_Tick(object sender, EventArgs e)
        {
            DisplayMessages();
        }
        #endregion

        #region Message List-View event-handler
        private void lstMessages_DoubleClick(object sender, EventArgs e)
        {
            // Clears the content of the Message List-View
            //
            btnMsgClear_Click(this, new EventArgs());
        }
        #endregion

        #region Information List-Box event-handler
        private void lbxInfo_DoubleClick(object sender, EventArgs e)
        {
            // Clears the content of the Information List-Box
            //
            btnInfoClear_Click(this, new EventArgs());
        }
        #endregion

        #region Textbox event handlers
        private void txtID_Leave(object sender, EventArgs e)
        {
            int iTextLength;
            uint uiMaxValue;

            // Calculates the text length and Maximum ID value according
            // with the Message Type
            //
            iTextLength = (chbExtended.Checked) ? 8 : 3;
            uiMaxValue = (chbExtended.Checked) ? (uint)0x1FFFFFFF : (uint)0x7FF;

            // The Textbox for the ID is represented with 3 characters for 
            // Standard and 8 characters for extended messages.
            // Therefore if the Length of the text is smaller than TextLength,  
            // we add "0"
            //
            while (txtID.Text.Length != iTextLength)
                txtID.Text = ("0" + txtID.Text);

            // We check that the ID is not bigger than current maximum value
            //
            if (Convert.ToUInt32(txtID.Text, 16) > uiMaxValue)
                txtID.Text = string.Format("{0:X" + iTextLength.ToString() + "}", uiMaxValue);
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            char chCheck;

            // We convert the Character to its Upper case equivalent
            //
            chCheck = char.ToUpper(e.KeyChar);

            // The Key is the Delete (Backspace) Key
            //
            if (chCheck == 8)
                return;
            // The Key is a number between 0-9
            //
            if ((chCheck > 47) && (chCheck < 58))
                return;
            // The Key is a character between A-F
            //
            if ((chCheck > 64) && (chCheck < 71))
                return;

            // Is neither a number nor a character between A(a) and F(f)
            //
            e.Handled = true;
        }

        private void txtData0_Leave(object sender, EventArgs e)
        {
            TextBox txtbCurrentTextbox;

            // all the Textbox Data fields are represented with 2 characters.
            // Therefore if the Length of the text is smaller than 2, we add
            // a "0"
            //
            if (sender.GetType().Name == "TextBox")
            {
                txtbCurrentTextbox = (TextBox)sender;
                while (txtbCurrentTextbox.Text.Length != 2)
                    txtbCurrentTextbox.Text = ("0" + txtbCurrentTextbox.Text);
            }
        }
        #endregion

        #region Radio- and Check- Buttons event-handlers
        private void chbShowPeriod_CheckedChanged(object sender, EventArgs e)
        {
            // According with the check-value of this checkbox,
            // the recieved time of a messages will be interpreted as 
            // period (time between the two last messages) or as time-stamp
            // (the elapsed time since windows was started)
            //
            lock (m_LastMsgsList.SyncRoot)
            {
                foreach (MessageStatus msg in m_LastMsgsList)
                    msg.ShowingPeriod = chbShowPeriod.Checked;
            }
        }

        private void chbExtended_CheckedChanged(object sender, EventArgs e)
        {
            uint uiTemp;

            txtID.MaxLength = (chbExtended.Checked) ? 8 : 3;

            // the only way that the text length can be bigger als MaxLength
            // is when the change is from Extended to Standard message Type.
            // We have to handle this and set an ID not bigger than the Maximum
            // ID value for a Standard Message (0x7FF)
            //
            if (txtID.Text.Length > txtID.MaxLength)
            {
                uiTemp = Convert.ToUInt32(txtID.Text, 16);
                txtID.Text = (uiTemp < 0x7FF) ? string.Format("{0:X3}", uiTemp) : "7FF";
            }

            txtID_Leave(this, new EventArgs());
        }

        private void chbRemote_CheckedChanged(object sender, EventArgs e)
        {
            TextBox txtbCurrentTextBox;

            txtbCurrentTextBox = txtData0;

            chbFD.Enabled = !chbRemote.Checked;

            // If the message is a RTR, no data is sent. The textboxes for data 
            // will be disabled
            // 
            for (int i = 0; i <= nudLength.Value; i++)
            {
                txtbCurrentTextBox.Enabled = !chbRemote.Checked;
                if (i < nudLength.Value)
                    txtbCurrentTextBox = (TextBox)this.Controls.Find("txtData" + i.ToString(), true)[0];
            }
        }

        private void chbFilterExt_CheckedChanged(object sender, EventArgs e)
        {
            int iMaxValue;

            iMaxValue = (chbFilterExt.Checked) ? 0x1FFFFFFF : 0x7FF;

            // We check that the maximum value for a selected filter 
            // mode is used
            //
            if (nudIdTo.Value > iMaxValue)
                nudIdTo.Value = iMaxValue;
            nudIdTo.Maximum = iMaxValue;

            if (nudIdFrom.Value > iMaxValue)
                nudIdFrom.Value = iMaxValue;
            nudIdFrom.Maximum = iMaxValue;
        }

        private void chbFD_CheckedChanged(object sender, EventArgs e)
        {
            chbRemote.Enabled = !chbFD.Checked;
            chbBRS.Enabled = chbFD.Checked;
            if (!chbBRS.Enabled)
                chbBRS.Checked = false;
            nudLength.Maximum = chbFD.Checked ? 15 : 8;
        }

        private void rdbTimer_CheckedChanged(object sender, EventArgs e)
        {
            if (!btnRelease.Enabled)
                return;

            // According with the kind of reading, a timer, a thread or a button will be enabled
            //
            if (rdbTimer.Checked)
            {
                // Abort Read Thread if it exists
                //
                if (m_ReadThread != null)
                {
                    m_ReadThread.Abort();
                    m_ReadThread.Join();
                    m_ReadThread = null;
                }

                // Enable Timer
                //
                tmrRead.Enabled = btnRelease.Enabled;
            }
            if (rdbManual.Checked)
            {
                // Abort Read Thread if it exists
                //
                if (m_ReadThread != null)
                {
                    m_ReadThread.Abort();
                    m_ReadThread.Join();
                    m_ReadThread = null;
                }
                // Disable Timer
                //
                tmrRead.Enabled = false;
            }
            btnRead.Enabled = btnRelease.Enabled && rdbManual.Checked;
        }

        private void chbCanFD_CheckedChanged(object sender, EventArgs e)
        {
            //       m_IsFD = chbCanFD.Checked;

            cbbBaudrates.Visible = !m_IsFD;
            laBaudrate.Visible = !m_IsFD;
            chbBRS.Visible = m_IsFD;

            if ((nudLength.Maximum > 8) && !m_IsFD)
                chbFD.Checked = false;
        }

        private void nudLength_ValueChanged(object sender, EventArgs e)
        {
            TextBox txtbCurrentTextBox;
            int iLength;

            txtbCurrentTextBox = txtData0;
            iLength = GetLengthFromDLC((int)nudLength.Value, !chbFD.Checked);
            laLength.Text = string.Format("{0} B.", iLength);

            for (int i = 0; i <= 64; i++)
            {
                txtbCurrentTextBox.Enabled = i <= iLength;
                if (i < 64)
                    txtbCurrentTextBox = (TextBox)this.Controls.Find("txtData" + i.ToString(), true)[0];
            }
        }
        #endregion

        #endregion

        #endregion
        int numarea = 2;
        private string nameAxisY = "";
        private string nameAxisY1 = "";
        private string nameAxisY2 = "";
        private void Form1_Load(object sender, EventArgs e)
        {
            UInt32 iBuffer;
            TPCANStatus stsResult;
            bool isFD;

            for (int i = 0; i < m_HandlesArray.Length; i++)
            {
                // Includes all no-Plug&Play Handles
                if (m_HandlesArray[i] <= PCANBasic.PCAN_DNGBUS1)
                {
                    //     cbbChannel.Items.Add(FormatChannelName(m_HandlesArray[i]));
                }

                else
                {
                    // Checks for a Plug&Play Handle and, according with the return value, includes it
                    // into the list of available hardware channels.
                    //
                    stsResult = PCANBasic.GetValue(m_HandlesArray[i], TPCANParameter.PCAN_CHANNEL_CONDITION, out iBuffer, sizeof(UInt32));
                    if ((stsResult == TPCANStatus.PCAN_ERROR_OK) && ((iBuffer & PCANBasic.PCAN_CHANNEL_AVAILABLE) == PCANBasic.PCAN_CHANNEL_AVAILABLE))
                    {
                        stsResult = PCANBasic.GetValue(m_HandlesArray[i], TPCANParameter.PCAN_CHANNEL_FEATURES, out iBuffer, sizeof(UInt32));
                        isFD = (stsResult == TPCANStatus.PCAN_ERROR_OK) && ((iBuffer & PCANBasic.FEATURE_FD_CAPABLE) == PCANBasic.FEATURE_FD_CAPABLE);
                    }
                }

                string strTemp;

                // Get the handle fromt he text being shown
                //
                //   strTemp = cbbChannel.Text;
                //  strTemp = strTemp.Substring(strTemp.IndexOf('(') + 1, 3);

                strTemp = "51h";   //另一個channel  52h
                strTemp = strTemp.Replace('h', ' ').Trim(' ');

                // Determines if the handle belong to a No Plug&Play hardware 
                //
                m_PcanHandle = Convert.ToUInt16(strTemp, 16);
                cbbBaudrates.SelectedIndex = 2;
            }
            DataGridViewRowCollection rows = dgvParam.Rows;
            rows.Add(new Object[] { "214h", "AIRead_1", 0 });   // 1080313
            rows.Add(new Object[] { "214h", "AIRead_2", 0 });
          //  rows.Add(new Object[] { "100h", "DIRead", 0 });
            dgvParam.AllowUserToAddRows = false;
            dgvParam.DefaultCellStyle.Font = new Font("新細明體", 10);

            dgvParam.Columns[0].Width = 100;//設定列寬度
            dgvParam.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvParam.Columns[1].Width = 250;//設定列寬度
           // dgvParam.Columns[2].Width = 150;//設定列寬度

            dgvParam.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvParam.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvParam.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvParam.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            dgvWrite.ColumnHeadersDefaultCellStyle.Font = new Font("新細明體", 10);

            DataGridViewRowCollection roww = dgvWrite.Rows;
            roww.Add(new Object[] { "  ", "Offset", 0 });
            roww.Add(new Object[] { "  ", "Gain", 0 });
          //  roww.Add(new Object[] { "100h", "DIRead", 0 });

            dgvWrite.AllowUserToAddRows = false;
            dgvWrite.DefaultCellStyle.Font = new Font("新細明體", 10);

            dgvWrite.Columns[0].Width = 80;//設定列寬度
            dgvWrite.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvWrite.Columns[1].Width = 250;//設定列寬度
          //  dgvWrite.Columns[2].Width = 100;//設定列寬度
            dgvWrite.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvWrite.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
          //  dgvWrite.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvWrite.ColumnHeadersDefaultCellStyle.Font = new Font("新細明體", 10);


            //  ================= Setup MSChart=======================================

            int SumOfHeights = (100 / numarea);
            //初始化設定多個 Serires & ChartArea
            for (int i = 0; i < numarea; i++)
            {
                chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(161)))), ((int)(((byte)(201)))));  //背景色
                chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;   //漸變方式
                chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(255))))); //圖表的邊框線條樣式
                chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                chart1.BorderlineWidth = 4;  //圖表邊框線條的寬度
                chart1.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Raised;  //圖表邊框的皮膚

                string seriresname = "Default" + i.ToString();
                chart1.Series.Add(seriresname);
                chart1.ChartAreas.Add(seriresname);
                chart1.Series[i].ChartArea = seriresname;
                chart1.Series[i].ChartType = SeriesChartType.Line;
                chart1.Series[i].IsVisibleInLegend = false;

                chart1.ChartAreas[i].AxisX.IsLabelAutoFit = false;
                chart1.ChartAreas[i].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
                chart1.ChartAreas[i].AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                chart1.ChartAreas[i].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                chart1.ChartAreas[i].AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;

                chart1.ChartAreas[i].AxisX.MajorGrid.Enabled = false;
                chart1.ChartAreas[i].AxisX.Maximum = 10D;
                chart1.ChartAreas[i].AxisX.Minimum = 0D;
                chart1.ChartAreas[i].AxisX.LabelStyle.Enabled = false;

                chart1.ChartAreas[i].BackColor = System.Drawing.Color.OldLace;
                chart1.ChartAreas[i].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
                chart1.ChartAreas[i].BackSecondaryColor = System.Drawing.Color.White;
                chart1.ChartAreas[i].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                chart1.ChartAreas[i].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                chart1.ChartAreas[i].ShadowColor = System.Drawing.Color.Transparent;

                //define inner plot position of the chart areas
                chart1.ChartAreas[i].InnerPlotPosition.Auto = true;
                chart1.ChartAreas[i].Position.Auto = true;

                //set our second chart area's alignments to match our first chart area
                chart1.ChartAreas[i].AlignmentOrientation = AreaAlignmentOrientations.Vertical;
                chart1.ChartAreas[i].AlignmentStyle = AreaAlignmentStyles.All;
                chart1.ChartAreas[i].AlignWithChartArea = chart1.ChartAreas[0].Name;  // ChartAreaX軸對齊


                chart1.ChartAreas[i].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
                chart1.ChartAreas[i].AxisY.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);

                chart1.ChartAreas[i].AxisX.MajorGrid.Enabled = false;
                chart1.ChartAreas[i].AxisX.MinorGrid.Enabled = false;
                chart1.ChartAreas[i].AxisY.MajorGrid.Enabled = false;
                chart1.ChartAreas[i].AxisY.MinorGrid.Enabled = false;
                chart1.ChartAreas[i].AxisX.LabelStyle.Enabled = false;

                chart1.ChartAreas[i].AxisY.TitleFont = new System.Drawing.Font("Trebuchet MS", 9.25F, System.Drawing.FontStyle.Bold);

                // Setting data point value as empty  form load show gird
                chart1.Series[i].Points.Add();
                chart1.Series[i].Points[0].IsEmpty = true;

                //放大縮小
                chart1.ChartAreas[i].AxisX.ScaleView.Zoomable = true;
                chart1.ChartAreas[i].CursorX.IsUserSelectionEnabled = true;
                chart1.ChartAreas[i].AxisX.ScaleView.SmallScrollSize = double.NaN;
                chart1.ChartAreas[i].AxisX.ScaleView.SmallScrollMinSize = 1;

                //設定ChartAreas的位置與大小
                chart1.ChartAreas[i].Position.Auto = false;
                chart1.ChartAreas[i].Position.X = 0;
                chart1.ChartAreas[i].Position.Y = SumOfHeights * i;
                chart1.ChartAreas[i].Position.Height = SumOfHeights;
                chart1.ChartAreas[i].Position.Width = 95;

            }
            chart1.ChartAreas[numarea - 1].Position.Auto = false;
            chart1.ChartAreas[numarea - 1].Position.X = 0;
            chart1.ChartAreas[numarea - 1].Position.Y = 100;
            chart1.ChartAreas[numarea - 1].Position.Height = SumOfHeights + numarea / 2;// 100 / (numarea - 2);
            chart1.ChartAreas[numarea - 1].Position.Width = 95;

            chart1.ChartAreas[numarea - 1].AxisX.LabelStyle.Enabled = true;
            chart1.ChartAreas[numarea - 1].AxisX.LabelStyle.Format = "0.00";  //1080617
            chart1.ChartAreas[numarea - 1].AxisX.Interval = 0.5D;  //1080617

            chart1.ChartAreas[0].AxisY.Maximum = 150D;
            chart1.ChartAreas[0].AxisY.Minimum = -150D;
            chart1.ChartAreas[0].AxisY.Title = nameAxisY;

            chart1.ChartAreas[1].AxisY.Maximum = 60D;
            chart1.ChartAreas[1].AxisY.Minimum = 0D;
            chart1.ChartAreas[1].AxisY.Title = nameAxisY1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  WriteDatattoFile();
        }

        private void dgvWrite_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvWrite_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            object updateName = senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value;
            string sID = "";

            if (e.RowIndex > -1) // Means that you've not clicked the column header
            {
                if (updateName.ToString() == "Gain")
                {
                    GlobalVarParameter.Gain = double.Parse(senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }

                if (updateName.ToString() == "Offset")
                {
                    GlobalVarParameter.Offset = double.Parse(senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }                
            }           

        }

        private void WriteData(string xID)
        {
            TPCANStatus stsResult;

            // Send the message
            //
            stsResult = WriteFrame();

            // The message was successfully sent
            //
            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                IncludeTextMessage("Message was successfully SENT");
            // An error occurred.  We show the error.
            //			
            else
                MessageBox.Show(GetFormatedError(stsResult));
        }
        private StreamWriter sw;

        private void button1_Click_1(object sender, EventArgs e)
        {
            double volt;
            volt = (GlobalVarParameter.Offset - Convert.ToDouble(105)) / GlobalVarParameter.Gain;
            txtVolt.Text = volt.ToString("f1");
        }
        private void chartUpdateTimer_Tick(object sender, EventArgs e)
        {
            msfactor = Convert.ToSingle(delay) / 1000.0;

            // Get new data from the queue and append them to the data arrays
            var packets = buffer.get();

            // if data arrays have insufficient space, we need to remove some old data.
            if (currentIndex + packets.Count >= sampleSize)
            {
                // For safety, we check if the queue contains too much data than the entire data arrays. If
                // this is the case, we only use the latest data to completely fill the data arrays.
                if (packets.Count > sampleSize)
                    packets = new ArraySegment<DataPacket>(packets.Array, packets.Count - sampleSize, sampleSize);

                // Remove oldest data to leave space for new data. To avoid frequent removal, we ensure at
                // least 5% empty space available after removal.
                int originalIndex = currentIndex;
                currentIndex = sampleSize * 95 / 100 - 1;
                if (currentIndex > sampleSize - packets.Count)
                    currentIndex = sampleSize - packets.Count;

                for (int i = 0; i < currentIndex; ++i)
                {
                    int srcIndex = i + originalIndex - currentIndex;
                    timeStamps[i] = timeStamps[srcIndex];
                    dataSeriesA[i] = dataSeriesA[srcIndex];
                    dataSeriesB[i] = dataSeriesB[srcIndex];
                    dataSeriesC[i] = dataSeriesC[srcIndex];
                }
            }

            // Append the data from the queue to the data arrays
            for (int n = packets.Offset; n < packets.Offset + packets.Count; ++n)
            {
                DataPacket p = packets.Array[n];
                timeStamps[currentIndex] = p.elapsedTime;
                dataSeriesA[currentIndex] = p.series0;
                dataSeriesB[currentIndex] = p.series1;
                dataSeriesC[currentIndex] = p.series2;
                ++currentIndex;

                lbxInfo.Items.Insert(0, p.elapsedTime);
                var t = DateTime.MinValue.AddSeconds(p.elapsedTime).ToString("HH:mm:ss.fff");  //HH大寫表示24小時制; hh小寫表示12小時制
                TimeSpan ts = TimeSpan.ParseExact(t, @"hh\:mm\:ss\.FFF", null);

                string strtime = ts.ToString(@"hh\:mm\:ss\.FFF");  // 將TimeSpen轉成string              

                if (sw.BaseStream != null)
                {
                    //  string time = DateTime.Now.ToString("HH:mm:ss:fff");                   
                    sw.WriteLine("elapsedTime = {0:0.0000} ,   series0 = {1:0.00} ,series1 = {2:0.00} ", p.elapsedTime, p.series0, p.series1, p.series2);
                }


                //chart1.Series[numarea - 3].Points.AddXY(p.elapsedTime, p.series0);
                chart1.Series[numarea - 2].Points.AddXY(p.elapsedTime, p.series1);
                chart1.Series[numarea - 1].Points.AddXY(p.elapsedTime, p.series2);

                // Adjust Y & X axis scale 如示波器一樣一直從右邊更新並即時X軸修正
                chart1.ResetAutoValues();
                newX++;

                // Adjust Y & X axis scale
                chart1.ResetAutoValues();

                for (int i = 0; i < numarea; i++)
                {
                    if (chart1.ChartAreas[i].AxisX.Maximum < newX * msfactor)
                    {
                        chart1.ChartAreas[i].AxisX.Maximum = newX * msfactor;
                    }
                    while (chart1.Series[i].Points.Count > numberOfPointsInChart)
                    {
                        // Remove data points on the left side
                        while (chart1.Series[i].Points.Count > numberOfPointsAfterRemoval)
                        {
                            chart1.Series[i].Points.RemoveAt(0);  //Chart左邊刪除的數據index 
                        }
                        // Adjust X axis scale
                        chart1.ChartAreas[i].AxisX.Minimum = p.elapsedTime - (numberOfPointsAfterRemoval) * msfactor;  //1080624
                        chart1.ChartAreas[i].AxisX.Maximum = chart1.ChartAreas[i].AxisX.Minimum + numberOfPointsInChart * msfactor;
                    }
                }
            }
        }
    }

    class GlobalVarParameter
    {
        //=======CAN-ID-0x214=========//
        public static short TCU_ChkSum;               //  output  :   TCU for MCU Tor cmd                     //TCU-CAN-4bit
        public static short TCU_RolCnt;             //  output  :   TCU for MCU Tor cmd                     //TCU-CAN-4bit
        public static UInt32 TCU_ActuatorPos;      //  output  :   TCU for MCU Tor cmd                     //TCU-CAN-17bit  //1080625

        public static short TCU_Tor_slew_rate;      //  output  :   TCU for MCU Tor cmd                     //TCU-CAN-10bit
        public static short TCU_shift_status;       //  output  :   TCU for anyone shift status             //TCU-CAN-3bit
        public static short TCU_Gear_status;        //  output  :   TCU for anyone Gear status              //TCU-CAN-2bit
        public static short TCU_to_MCU_control_mode;//  output  :   TCU for MCU control mode                //TCU-CAN-1bit
        public static short TCU_error_code;         //  output  :   TCU for MCU AVL error code              //TCU-CAN-3bit

        public static short TCU_VDC_Bus;         //  output  :   TCU for MCU AVL Vdc Bus              //TCU-CAN-4bit
        public static short TCU_Gear_status_PUMA;         //  output  :   TCU for MCU AVL Vdc Bus              //TCU-CAN-4bit
        //============Record============//

        //=======CAN-ID-0x224=========//
        public static short AIRead_1;       //  input   :   AVL for TCU switch Gear mode            //HMI-RS232-1bit
        public static short AIRead_2;       //  input   :   AVL for TCU switch Gear mode            //HMI-RS232-1bit
        public static short DIRead;           //  input   :   AVL for TCU Gear_cmd                    //AVL-CAN-2bit

        public static double Gain=0.111;
        public static double Offset=2117;

        public static double Volt;       //  input   :   AVL for TCU switch Gear mode            //HMI-RS232-1bit
        public static double Current;
    }
}
