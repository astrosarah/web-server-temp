﻿///////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2016 Advanced Software Engineering Limited
//
// You may use and modify the code in this file in your application, provided the code and
// its modifications are used only in conjunction with ChartDirector. Usage of this software
// is subjected to the terms and condition of the ChartDirector license.
///////////////////////////////////////////////////////////////////////////////////////////////////   

using System;
using System.Threading;
using System.Diagnostics;
using ICDIBasic;

namespace ChartDirectorSampleCode
{
    class RandomWalk
    {
        // The callback function to handle the generated data
        public delegate void DataHandler(double elapsedTime, double series0, double series1, double series2);
        private DataHandler handler;

        // Random number genreator thread
        Thread pingThread;
        private bool stopThread;
        
        // The period of the data series in milliseconds. This random series implementation just use the 
        // windows timer for timing. In many computers, the default windows timer resolution is 1/64 sec,
        // or 15.6ms. This means the interval may not be exactly accurate.
        const int interval = 10;

        public RandomWalk(DataHandler handler)
        {
            this.handler = handler;
        }

        //
        // Start the random generator thread
        //        
        public void start()
        {
            if (null != pingThread)
                return;

            pingThread = new Thread(threadProc);
            pingThread.Start();            
        }

        //
        // Stop the random generator thread
        //
        public void stop()
        {
            stopThread = true;
            if (null != pingThread)
                pingThread.Join();
            pingThread = null;
            stopThread = false;
        }

        //
        // The random generator thread
        //
        void threadProc(object obj)
        {
            long currentTime = 0;

            // Random walk variables
            Random rand = new Random(9);
            double series0 = 32;
            double series1 = 63;
            double series2 = 63;
            double scaleFactor = Math.Sqrt(interval * 0.1);

            // Variables to keep track of the timing
            Stopwatch swtimer = new Stopwatch();
            swtimer.Start();
                        
            while (!stopThread)
            {
                // Compute the next data value
                currentTime = swtimer.Elapsed.Ticks / 10000;

                //series0 = rand.Next(0,3000);
                //series1 = rand.Next(0, 50);
                //series2 = rand.Next(0, 100);

                series2 = GlobalVarParameter.Volt;
                series1 = GlobalVarParameter.Current;

                series0 = rand.Next(0, 5);

                // Call the handler
                handler(currentTime / 1000.0, series0, series1,series2);

                // Sleep until next walk                
                Thread.Sleep((int)(interval));
            }
        }
    }
}

